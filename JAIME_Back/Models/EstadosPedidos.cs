﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public static class EstadosPedidos
    {
        public const string SinConfirmar = "Sin confirmar";
        public const string Confirmado = "Confirmado";
        public const string EnCamino = "En Camino"; //En camino al comercio
        public const string Retirado = "Retirado";
        public const string Entregado = "Entregado";
        public const string Cancelado = "Cancelado";
    }
}