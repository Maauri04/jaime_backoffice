﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class MediosPagoViewModel
    {
        public string idComercio { get; set; }
        public bool AceptaDebito { get; set; }
        public bool AceptaCredito { get; set; }
        public bool AceptaMercadopago { get; set; }
        public bool AceptaEfectivo { get; set; }
        public bool AceptaTransferencia { get; set; }
    }
}