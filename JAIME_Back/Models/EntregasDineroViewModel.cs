﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Datos;

namespace JAIME_Back.Models
{
    public class EntregasDineroViewModel
    {
        public int Id { get; set; }
        public string Delivery { get; set; }
        public int? DineroEntregado { get; set; }
        public int? TotalPedido { get; set; }
        public int? Cobranza { get; set; }
        public DateTime? Fecha { get; set; }
        public int IdPedido { get; set; }
    }
}