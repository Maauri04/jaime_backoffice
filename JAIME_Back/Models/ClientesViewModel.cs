﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class ClientesViewModel
    {
        public string NombreApellido { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public int CantPedidos { get; set; }
    }
}