﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class GuardarEntregaViewModel
    {
        public int idPedido { get; set; }
        public int entrega { get; set; }
    }
}