﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace JAIME_Back.Models
{
    // Para agregar datos de perfil del usuario, agregue más propiedades a su clase ApplicationUser. Visite https://go.microsoft.com/fwlink/?LinkID=317594 para obtener más información.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            userIdentity.AddClaim(new Claim("TipoUsuario", this.TipoUsuario));
            return userIdentity;
        }

        public string Estado { get; set; }
        public string Direccion { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public int? IdRol { get; set; }
        public string Ubicacion { get; set; }
        public int? IdRubro { get; set; }
        public int? IdSubRubro { get; set; }
        public string TipoUsuario { get; set; }
        public int? PuntajeTotal { get; set; }
        public string PhoneNumber2 { get; set; }
        public string RazonSocial { get; set; }
        public string NombreFantasia { get; set; }
        public string CuitCuil { get; set; }
        public bool? EntregaLocal { get; set; }
        public decimal? PrecioEnvio { get; set; }
        public decimal? PrecioMinimo { get; set; }
        public string Poliza { get; set; }
        public DateTime? FechaAlta { get; set; }
        public DateTime? FechaBaja { get; set; }
        public DateTime? VencimientoPoliza { get; set; }
        public string UrlMapaUsuario { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}