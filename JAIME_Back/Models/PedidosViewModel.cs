﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class PedidosViewModel
    {
        public string Comercio { get; set; }
        public DateTime? Fecha { get; set; }
        public string Horario { get; set; }
        public string Direccion { get; set; }
        public string Estado { get; set; }
        public string idDelivery { get; set; }
        public string Delivery { get; set; }
        public string MedioPago { get; set; }
        public int? Monto { get; set; }
        public string Ubicacion { get; set; }
        public string Comentario { get; set; }
        public string Cliente { get; set; }
        public string Telefono { get; set; }
        public string Producto { get; set; }
    }
}