﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace JAIME_Back.Models
{
    public static class IdentityExtensions
    {
        public static string ObtenerTipoUsuario(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("TipoUsuario");
            // Test for null to avoid issues during local testing
            return (claim != null) ? claim.Value : string.Empty;
        }   
    }
}