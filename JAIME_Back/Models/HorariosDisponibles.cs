﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public static class HorariosDisponibles
    {
        //39 horarios distintos
        public static readonly List<HoraModel> HorariosTodos = new List<HoraModel>() {
            new HoraModel {Peso = 0, Hora = "08:00 hs" },
            new HoraModel {Peso = 1, Hora = "08:30 hs" },
            new HoraModel {Peso = 2, Hora = "09:00 hs" },
            new HoraModel {Peso = 3, Hora = "09:30 hs" },
            new HoraModel {Peso = 4, Hora = "10:00 hs" },
            new HoraModel {Peso = 5, Hora = "10:30 hs" },
            new HoraModel {Peso = 6, Hora = "11:00 hs" },
            new HoraModel {Peso = 7, Hora = "11:30 hs" },
            new HoraModel {Peso = 8, Hora = "12:00 hs" },
            new HoraModel {Peso = 9, Hora = "12:30 hs" },
            new HoraModel {Peso = 10, Hora = "13:00 hs" },
            new HoraModel {Peso = 11, Hora = "13:30 hs" },
            new HoraModel {Peso = 12, Hora = "14:00 hs" },
            new HoraModel {Peso = 13, Hora = "14:30 hs" },
            new HoraModel {Peso = 14, Hora = "15:00 hs" },
            new HoraModel {Peso = 15, Hora = "15:30 hs" },
            new HoraModel {Peso = 16, Hora = "16:00 hs" },
            new HoraModel {Peso = 17, Hora = "16:30 hs" },
            new HoraModel {Peso = 18, Hora = "17:00 hs" },
            new HoraModel {Peso = 19, Hora = "17:30 hs" },
            new HoraModel {Peso = 20, Hora = "18:00 hs" },
            new HoraModel {Peso = 21, Hora = "18:30 hs" },
            new HoraModel {Peso = 22, Hora = "19:00 hs" },
            new HoraModel {Peso = 23, Hora = "19:30 hs" },
            new HoraModel {Peso = 24, Hora = "20:00 hs" },
            new HoraModel {Peso = 25, Hora = "20:30 hs" },
            new HoraModel {Peso = 26, Hora = "21:00 hs" },
            new HoraModel {Peso = 27, Hora = "21:30 hs" },
            new HoraModel {Peso = 28, Hora = "22:00 hs" },
            new HoraModel {Peso = 29, Hora = "22:30 hs" },
            new HoraModel {Peso = 30, Hora = "23:00 hs" },
            new HoraModel {Peso = 31, Hora = "23:30 hs" },
            new HoraModel {Peso = 32, Hora = "00:00 hs" },
            new HoraModel {Peso = 33, Hora = "00:30 hs" },
            new HoraModel {Peso = 34, Hora = "01:00 hs" },
            new HoraModel {Peso = 35, Hora = "01:30 hs" },
            new HoraModel {Peso = 36, Hora = "02:00 hs" },
            new HoraModel {Peso = 37, Hora = "02:30 hs" },
            new HoraModel {Peso = 38, Hora = "03:00 hs" }
        };
    }       
}           
            