﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class GuardarEstadoPedidoViewModel
    {
        public int idPedido { get; set; }
        public string estadoPedido { get; set; }
        public string medioPago { get; set; }
        public int? monto { get; set; }
        public string idDelivery { get; set; }
        public DateTime fecha { get; set; }
    }
}