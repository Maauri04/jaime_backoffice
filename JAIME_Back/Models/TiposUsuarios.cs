﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public static class TiposUsuarios
    {
        public static string Comercio { get; set; } = "Comercio";
        public static string Usuario { get; set; } = "Usuario";
        public static string Coordinador { get; set; } = "Coordinador";
        public static string Administrador { get; set; } = "Administrador";
        public static string Delivery { get; set; } = "Delivery";
    }
}