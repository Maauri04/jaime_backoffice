﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public static class MediosPago
    {
        public static string Efectivo { get; } = "Efectivo";
        public static string Debito { get; } = "Débito";
        public static string Credito { get; } = "Crédito";
        public static string MercadoPago { get; } = "Mercado Pago";
        public static string Transferencia { get; } = "Transferencia";
    }
}