﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class BackOfficeViewModel
    {
        public List<Pedidos> ListaPedidos = new List<Pedidos>();
        public List<AspNetUsers> ComerciosSinHorarios = new List<AspNetUsers>();
        public List<AspNetUsers> ListaComercios = new List<AspNetUsers>();
        public List<MediosPagoComercios> MediosPago = new List<MediosPagoComercios>();
    }
}