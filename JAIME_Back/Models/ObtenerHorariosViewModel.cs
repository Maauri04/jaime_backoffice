﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class ObtenerHorariosViewModel // Se utiliza para obtener los horarios disponibles a realizar en un pedido
    {
        public string Fecha { get; set; }
        public int Dia { get; set; }
    }
}