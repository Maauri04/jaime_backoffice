﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public static class EstadosUsuarios
    {
        public static string UsuarioActivo { get; set; } = "Activo";
        public static string UsuarioInActivo { get; set; } = "Inactivo";
    }
}