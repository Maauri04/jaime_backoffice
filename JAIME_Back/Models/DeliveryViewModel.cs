﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Datos;

namespace JAIME_Back.Models
{
    public class DeliveryViewModel
    {
        public int Id { get; set; }
        public string IdComercio { get; set; }
        public DateTime? Fecha { get; set; }
        public string Horario { get; set; }
        public string Direccion { get; set; }
        public string Estado { get; set; }
        public string IdDelivery { get; set; }
        public string MedioPago { get; set; }
        public int? Monto { get; set; }
        public string Ubicacion { get; set; }
        public string Comentario { get; set; }
        public string Cliente { get; set; }
        public string Telefono { get; set; }
        public string Producto { get; set; }
        public int? Prioridad { get; set; }
        public string UrlMapaComercio { get; set; }
        public string Comercio { get; set; }
        public string DireccionComercio { get; set; }
    }
}