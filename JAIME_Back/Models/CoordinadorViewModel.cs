﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class CoordinadorViewModel
    {
        public List<Pedidos> ListaPedidos = new List<Pedidos>();
        public List<AspNetUsers> ListaComercios = new List<AspNetUsers>();
        public List<AspNetUsers> ListaDeliveries = new List<AspNetUsers>();
    }
}