﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class HorariosViewModels
    {
        public string IdComercio { get; set; }
        public string DiaAtencion { get; set; }
        public string HoraDesde { get; set; }
        public string HoraHasta { get; set; }

    }
}