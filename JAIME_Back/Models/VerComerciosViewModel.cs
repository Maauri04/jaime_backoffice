﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class VerComerciosViewModel
    {
        public string Direccion { get; set; }
        public string Email { get; set; }
        public int CantPedidos { get; set; }
        public string RazonSocial { get; set; }
        public string NombreFantasia { get; set; }
    }
}