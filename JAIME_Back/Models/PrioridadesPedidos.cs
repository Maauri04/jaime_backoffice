﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public static class PrioridadesPedidos
    {
        public const int SinConfirmar = 6;
        public const int Confirmado = 5;
        public const int EnCamino = 4; //En camino al comercio
        public const int Retirado = 3;
        public const int Entregado = 2;
        public const int Cancelado = 1;
    }
}