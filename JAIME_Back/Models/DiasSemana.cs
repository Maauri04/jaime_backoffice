﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class DiasSemana
    {
        public static string Lunes { get; } = "lunes";
        public static string Martes { get; } = "martes";
        public static string Miercoles { get; } = "miercoles";
        public static string Jueves { get; } = "jueves";
        public static string Viernes { get; } = "viernes";
        public static string Sabado { get; } = "sabado";
        public static string Domingo { get; } = "domingo";
    }
}