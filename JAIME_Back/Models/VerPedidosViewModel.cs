﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JAIME_Back.Models
{
    public class VerPedidosViewModel
    {
        public List<Pedidos> Pedidos = new List<Pedidos>();
        public List<MediosPagoComercios> MediosPago = new List<MediosPagoComercios>();
        public int PedidosHoy { get; set; }
        public int GananciasTotales { get; set; }
        public decimal EnviosCompletados { get; set; }
        public int PedidosPendientes { get; set; }
    }
}