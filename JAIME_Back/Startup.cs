﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JAIME_Back.Startup))]
namespace JAIME_Back
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
