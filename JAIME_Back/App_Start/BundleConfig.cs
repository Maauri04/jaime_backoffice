﻿using System.Web;
using System.Web.Optimization;

namespace JAIME_Back
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/vendor/jquery/jquery.min.js",
                        "~/Content/vendor/bootstrap/js/bootstrap.bundle.min.js",
                        "~/Content/vendor/jquery-easing/jquery.easing.min.js",
                        "~/Content/vendor/js/sb-admin-2.min.js",
                        "~/Content/vendor/vendor/chart.js/Chart.min.js",
                        "~/Content/vendor/js/demo/chart-area-demo.js",
                        //"~/Content/js/demo/chart-pie-demo.js",
                        "~/Content/vendor/datatables/jquery.dataTables.js",
                        "~/Content/vendor/datatables/dataTables.bootstrap4.min.js",
                        "~/Content/js/demo/datatables-demo.js",
                        "~/Content/js/sweetalert2.all.min.js",
                        "~/Content/js/funciones-utiles.js"));


            bundles.Add(new StyleBundle("~/bundles/css").Include(
                         "~/Content/vendor/fontawesome-free/css/all.min.css",
                         "~/Content/css/sb-admin-2.min.css",
                         "~/Content/vendor/datatables/dataTables.bootstrap4.min.css",
                         "~/Content/css/sweetalert2.min.css",
                         "~/Content/css/estilos-utiles.css"));
        }
    }
}
