﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datos;
using JAIME_Back.Models;

namespace JAIME_BackOffice.Controllers
{
    public class DeliveryController : Controller
    {
        jaimeDBEntities db = new jaimeDBEntities();

        public ActionResult Index(bool? mostrarEntregados = false) //Listado de pedidos del delivery
        {
            List<DeliveryViewModel> model = new List<DeliveryViewModel>();
            string idUsuario = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault().Id;
            List<Pedidos> pedidosDelivery = db.Pedidos.Where(x => x.IdDelivery == idUsuario && x.Estado != EstadosPedidos.SinConfirmar).OrderByDescending(x => x.Prioridad).ThenBy(x => x.Fecha).ThenBy(x => x.Horario).ToList();
            foreach (var item in pedidosDelivery)
            {
                model.Add(new DeliveryViewModel
                {
                    Id = item.Id,
                    IdComercio = item.IdComercio,
                    IdDelivery = item.IdDelivery,
                    MedioPago = item.MedioPago,
                    Monto = item.Monto,
                    Prioridad = item.Prioridad,
                    Producto = item.Producto,
                    Telefono = item.Telefono,
                    Ubicacion = item.Ubicacion,
                    UrlMapaComercio = db.AspNetUsers.Where(x => x.Id == item.IdComercio).FirstOrDefault().UrlMapaUsuario,
                    Cliente = item.Cliente,
                    Comentario = item.Comentario,
                    Direccion = item.Direccion,
                    Estado = item.Estado,
                    Fecha = item.Fecha,
                    Horario = item.Horario,
                    Comercio = db.AspNetUsers.Where(x => x.Id == item.IdComercio).FirstOrDefault().NombreFantasia,
                    DireccionComercio = db.AspNetUsers.Where(x => x.Id == item.IdComercio).FirstOrDefault().Direccion
                });
            }
            if (mostrarEntregados != null && mostrarEntregados == false)
            {
                model = model.Where(x => x.Estado != EstadosPedidos.Entregado).ToList();
            }
            ViewBag.mostrarEntregados = mostrarEntregados;
            return View(model);
        }

        public ActionResult ControlCaja() //Ve la tabla de entregas dinero de coordinador (solo lectura)
        {
            return View();
        }

        public ActionResult Estadisticas() //Detalle completo de un pedido
        {
            return View();
        }
    }
}