﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JAIME_Back.Models;
using Datos;

namespace JAIME_BackOffice.Controllers
{
    public class CoordinadorController : Controller
    {
        jaimeDBEntities db = new jaimeDBEntities();
        public ActionResult Index(string fechaDesde, string fechaHasta, string cliente, string comercio, string estado) //Dashboard del coordinador
        {
            CoordinadorViewModel model = new CoordinadorViewModel();
            model.ListaComercios = db.AspNetUsers.Where(x => x.TipoUsuario == TiposUsuarios.Comercio).ToList();
            model.ListaDeliveries = db.AspNetUsers.Where(x => x.TipoUsuario == TiposUsuarios.Delivery).ToList();
            model.ListaPedidos = db.Pedidos.ToList();
            if (!string.IsNullOrEmpty(fechaDesde) && !string.IsNullOrEmpty(fechaHasta))
            {
                ViewBag.fechaDesde = fechaDesde; //Los uso para pre cargar los filtros a la vuelta
                ViewBag.fechaHasta = fechaHasta;
                DateTime fecDesde = Convert.ToDateTime(fechaDesde);
                DateTime fecHasta = Convert.ToDateTime(fechaHasta);
                model.ListaPedidos = model.ListaPedidos.Where(x => x.Fecha >= fecDesde && x.Fecha <= fecHasta).ToList();
            }
            if (!string.IsNullOrEmpty(cliente))
            {
                ViewBag.cliente = cliente;
                model.ListaPedidos = model.ListaPedidos.Where(x => x.Cliente.ToLower() == cliente.ToLower() || x.Cliente.ToLower().Contains(cliente.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(comercio))
            {
                ViewBag.comercio = comercio;
                if (comercio != "Todos")
                {
                    model.ListaPedidos = model.ListaPedidos.Where(x => x.IdComercio == comercio).ToList();
                }
            }
            if (!string.IsNullOrEmpty(estado))
            {
                ViewBag.estado = estado;
                if (estado != "Todos")
                {
                    model.ListaPedidos = model.ListaPedidos.Where(x => x.Estado == estado).ToList();
                }
            }
            model.ListaPedidos = model.ListaPedidos.OrderByDescending(x => x.Prioridad).ThenByDescending(x => x.Fecha).ThenBy(x => x.Horario).ToList();
            return View(model);
        }

        public ActionResult FiltrarPedidos(DateTime? fechaDesde, DateTime? fechaHasta, string cliente, string comercio, string estado)
        {

            return View();
        }

        public ActionResult AsignarDeliveryDia() //Asigna los deliveries que se utilizaran en la asignacion de pedidos del dia.
        {
            return View();
        }

        public ActionResult GuardarDeliveryPedido(GuardarDeliveryPedido data)
        {
            try
            {
                Pedidos pedido = db.Pedidos.Where(x => x.Id == data.idPedido).FirstOrDefault();
                pedido.IdDelivery = data.idDelivery;
                db.Entry(pedido).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Json(new { });
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult MapaComercio()
        {
            List<AspNetUsers> model = db.AspNetUsers.Where(x => x.TipoUsuario == TiposUsuarios.Comercio).ToList();
            return View(model);
        }

        public ActionResult AsignarMapaComercio(AsignarMapaViewModel data) //Asigna los deliveries que se utilizaran en la asignacion de pedidos del dia.
        {
            try
            {
                AspNetUsers usuario = db.AspNetUsers.Where(x => x.Id == data.idUsuario).FirstOrDefault();
                data.urlMap = data.urlMap.Replace("width=\"400\"", "width=\"300\"");
                data.urlMap = data.urlMap.Replace("width=\"600\"", "width=\"300\"");
                data.urlMap = data.urlMap.Replace("width=\"800\"", "width=\"300\"");
                data.urlMap = data.urlMap.Replace("height=\"450\"", "height=\"300\"");
                data.urlMap = data.urlMap.Replace("height=\"600\"", "height=\"300\"");
                usuario.UrlMapaUsuario = data.urlMap;
                db.Entry(usuario).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Json(new { });
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult DetallePedido() //Detalle completo de un pedido
        {
            return View();
        }

        public ActionResult EntregasDineroDelivery(string fechaDesde, string fechaHasta) //Se lleva un control de las entregas de dinero de los deliveries que cobran pedidos en efectivo
        {
            try
            {
                List<EntregasDineroDeliveries> ListaEntregasDinero = new List<EntregasDineroDeliveries>();
                ListaEntregasDinero = db.EntregasDineroDeliveries.ToList();
                List<EntregasDineroViewModel> model = new List<EntregasDineroViewModel>();
                foreach (var item in ListaEntregasDinero)
                {
                    AspNetUsers delivery = db.AspNetUsers.Where(x => x.Id == item.IdDelivery).FirstOrDefault();
                    model.Add(new EntregasDineroViewModel
                    {
                        Id = item.Id,
                        Delivery = delivery.Nombre + " " + delivery.Apellido,
                        DineroEntregado = item.DineroEntregado,
                        TotalPedido = item.TotalPedido,
                        Cobranza = item.TotalPedido - item.DineroEntregado,
                        Fecha = item.Fecha,
                        IdPedido = item.IdPedido
                    });
                }
                //FILTROS
                if (!string.IsNullOrEmpty(fechaDesde) && !string.IsNullOrEmpty(fechaHasta))
                {
                    ViewBag.fechaDesde = fechaDesde; //Los uso para pre cargar los filtros a la vuelta
                    ViewBag.fechaHasta = fechaHasta;
                    DateTime fecDesde = Convert.ToDateTime(fechaDesde);
                    DateTime fecHasta = Convert.ToDateTime(fechaHasta);
                    model = model.Where(x => x.Fecha >= fecDesde && x.Fecha <= fecHasta).ToList();
                }

                model = model.OrderByDescending(x => x.Cobranza).ToList();

                return View(model);
            }
            catch (Exception)
            {
                throw;
            }           
        }

        public ActionResult GuardarEntrega(GuardarEntregaViewModel data) //Se guarda una nueva entrega
        {
            try
            {
                bool exito = false;
                string message = "¡La entrega supera el monto total del pedido!";
                EntregasDineroDeliveries entrega = db.EntregasDineroDeliveries.Where(x => x.IdPedido == data.idPedido).FirstOrDefault();
                entrega.DineroEntregado = entrega.DineroEntregado + data.entrega;
                if (entrega.DineroEntregado > entrega.TotalPedido)
                {
                    return Json(new { message });
                }
                else
                {
                    exito = true;
                    db.Entry(entrega).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { exito });
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult ResumenCuenta() //Se carga el resumen de cuenta
        {
            return View();
        }
    }
}