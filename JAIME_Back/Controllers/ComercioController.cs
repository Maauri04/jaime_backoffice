﻿using Datos;
using JAIME_Back.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JAIME_BackOffice.Controllers
{
    public class ComercioController : Controller
    {
        jaimeDBEntities db = new jaimeDBEntities();

        public ActionResult Index(string fechaDesde, string fechaHasta, string cliente, string estado) //Dashboard Comercio. Resumen del comercio. 
        {
            VerPedidosViewModel model = new VerPedidosViewModel();
            string idComercio = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).FirstOrDefault().Id;
            model.Pedidos = db.Pedidos.Where(x => x.IdComercio == idComercio).ToList();
            if (!string.IsNullOrEmpty(fechaDesde) && !string.IsNullOrEmpty(fechaHasta))
            {
                ViewBag.fechaDesde = fechaDesde; //Los uso para pre cargar los filtros a la vuelta
                ViewBag.fechaHasta = fechaHasta;
                DateTime fecDesde = Convert.ToDateTime(fechaDesde);
                DateTime fecHasta = Convert.ToDateTime(fechaHasta);
                model.Pedidos = model.Pedidos.Where(x => x.Fecha >= fecDesde && x.Fecha <= fecHasta).ToList();
            }
            if (!string.IsNullOrEmpty(cliente))
            {
                ViewBag.cliente = cliente;
                model.Pedidos = model.Pedidos.Where(x => x.Cliente.ToLower() == cliente.ToLower() || x.Cliente.ToLower().Contains(cliente.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(estado))
            {
                ViewBag.estado = estado;
                if (estado != "Todos")
                {
                    model.Pedidos = model.Pedidos.Where(x => x.Estado == estado).ToList();
                }
            }
            model.Pedidos = model.Pedidos.OrderByDescending(x => x.Prioridad).ThenByDescending(x => x.Fecha).ThenBy(x => x.Horario).ToList();
           
            //Estadisticas tarjetas
            model.PedidosHoy = model.Pedidos.Where(x => Convert.ToDateTime(x.Fecha).ToShortDateString() == DateTime.Now.ToShortDateString()).Count();
            model.GananciasTotales = 0;
            foreach (var item in model.Pedidos)
            {
                if (item.Estado == EstadosPedidos.Entregado)
                {
                    model.GananciasTotales += Convert.ToInt32(item.Monto);
                }
            }
            model.EnviosCompletados = 0;
            int cantPedidosEntregados = model.Pedidos.Where(x => x.Estado == EstadosPedidos.Entregado).Count();
            int cantPedidosTotales = model.Pedidos.Where(x => x.Estado != EstadosPedidos.Cancelado).Count();
            if (cantPedidosTotales > 0)
            {
                model.EnviosCompletados = (cantPedidosEntregados * 100) / cantPedidosTotales;
            }
            model.PedidosPendientes = model.Pedidos.Where(x => x.Estado == EstadosPedidos.SinConfirmar || x.Estado == EstadosPedidos.Confirmado || x.Estado == EstadosPedidos.Retirado || x.Estado == EstadosPedidos.EnCamino).Count();
            return View(model);
        }

        public ActionResult ObtenerClientes()
        {
            string idComercio = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault().Id;
            List<Pedidos> pedidos = db.Pedidos.Where(x => x.IdComercio == idComercio).ToList();
            List<string> clientes = new List<string>();
            foreach (var item in pedidos)
            {
                if (!string.IsNullOrEmpty(item.Cliente))
                {
                    clientes.Add(item.Cliente);
                }
            }
            clientes = clientes.Distinct().ToList();
            return Json(new { clientes });
        }

        public ActionResult ObtenerClientePorNombre(string cli)
        {
            string idComercio = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault().Id;
            List<Pedidos> pedidos = db.Pedidos.Where(x => x.IdComercio == idComercio).ToList();
            string direccion = "";
            string telefono = "";
            foreach (var item in pedidos)
            {
                if (!string.IsNullOrEmpty(item.Cliente))
                {
                    if (item.Cliente == cli)
                    {
                        direccion = item.Direccion;
                        telefono = item.Telefono;
                    }
                }
            }
            return Json(new { direccion, telefono });
        }

        public ActionResult VerClientes()
        {
            VerUsuariosViewModel model = new VerUsuariosViewModel();
            string idComercio = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault().Id;
            List<Pedidos> pedidosComercio = db.Pedidos.Where(x => x.IdComercio == idComercio).ToList();
            foreach (var item in pedidosComercio)
            {
                ClientesViewModel cliente = new ClientesViewModel
                {
                    NombreApellido = item.Cliente,
                    Direccion = item.Direccion,
                    Telefono = item.Telefono,
                    CantPedidos = pedidosComercio.Where(x => x.Cliente == item.Cliente).Count()
                };
                model.ListaClientes.Add(cliente);
            }
            model.ListaClientes = model.ListaClientes.GroupBy(customer => customer.NombreApellido).Select(group => group.First()).OrderByDescending(x => x.CantPedidos).ToList();
            return View(model);
        }

        public ActionResult ObtenerHorariosPorDia(ObtenerHorariosViewModel data)
        {
            string diaSemana = null;
            switch (data.Dia)
            {
                case 0:
                    diaSemana = DiasSemana.Lunes;
                    break;
                case 1:
                    diaSemana = DiasSemana.Martes;
                    break;
                case 2:
                    diaSemana = DiasSemana.Miercoles;
                    break;
                case 3:
                    diaSemana = DiasSemana.Jueves;
                    break;
                case 4:
                    diaSemana = DiasSemana.Viernes;
                    break;
                case 5:
                    diaSemana = DiasSemana.Sabado;
                    break;
                case 6:
                    diaSemana = DiasSemana.Domingo;
                    break;
                default:
                    break;
            }
            List<HorariosComercios> HorariosComercio = new List<HorariosComercios>();
            string idComercio = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault().Id;
            HorariosComercio = db.HorariosComercios.Where(x => x.DiaAtencion == diaSemana && x.IdComercio == idComercio).ToList();
            List<string> horariosDisponibles = new List<string>();

            string horaDesde;
            string horaHasta;
            List<HoraModel> horariosAgregar = new List<HoraModel>();
            foreach (var hora in HorariosComercio) //Formateo hora inicial y final para comparar
            {
                horaDesde = hora.HoraDesde;
                horaHasta = hora.HoraHasta;
                int pesoDesde = HorariosDisponibles.HorariosTodos.Where(x => x.Hora == horaDesde).FirstOrDefault().Peso;
                int pesoHasta = HorariosDisponibles.HorariosTodos.Where(x => x.Hora == horaHasta).FirstOrDefault().Peso;
                DateTime fechaSel = Convert.ToDateTime(data.Fecha);
                string fechaHoy = Convert.ToDateTime(DateTime.Now).ToShortDateString();
                if (DateTime.Compare(fechaSel, Convert.ToDateTime(fechaHoy)) == 0) //si la fecha seleccionada es hoy
                {
                    int diferenciaHoraria = 4; //EQUILIBRIO DIFERENCIAS DE HORA CON EL SERVIDOR (PST a GMT) son 4 hs
                    string horaActual = (DateTime.Now.Hour + diferenciaHoraria).ToString();
                    string minutoActual = DateTime.Now.Minute.ToString();
                    if (Convert.ToInt32(minutoActual) <= 30)
                    {
                        minutoActual = "30";
                    }
                    else
                    {
                        if (Convert.ToInt32(minutoActual) > 30)
                        {
                            minutoActual = "00";
                            horaActual = Convert.ToString(Convert.ToInt32(horaActual) + 1);
                        }
                    }
                    horaActual = Convert.ToInt32(horaActual) < 10 ? "0" + horaActual : horaActual;
                    horaActual += ":" + minutoActual + " hs";
                    int pesoHoraActual = HorariosDisponibles.HorariosTodos.Where(x => x.Hora == horaActual).FirstOrDefault().Peso;

                    horariosAgregar = HorariosDisponibles.HorariosTodos.Where(x => x.Peso >= pesoDesde && x.Peso <= pesoHasta && x.Peso >= pesoHoraActual).ToList();
                }
                else
                {
                    if (DateTime.Compare(fechaSel, Convert.ToDateTime(fechaHoy)) < 0)
                    {
                        return Json(new { });
                    }
                    horariosAgregar = HorariosDisponibles.HorariosTodos.Where(x => x.Peso >= pesoDesde && x.Peso <= pesoHasta).ToList();
                }
                foreach (var item in horariosAgregar)
                {
                    horariosDisponibles.Add(item.Hora);
                }
            }
            return Json(new { horariosDisponibles });
        }

        public ActionResult VerPedidos() //Enfocado a traer pedidos y estadisticas de ellos.
        {
            VerPedidosViewModel model = new VerPedidosViewModel();
            string idComercio = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).FirstOrDefault().Id;
            model.Pedidos = db.Pedidos.Where(x => x.IdComercio == idComercio).OrderByDescending(x => x.Fecha).ToList();
            return View(model);
        }

        public ActionResult ResumenCuenta() //Ver el resumen de la cuenta
        {
            return View();
        }

        public ActionResult Estadisticas() //Ver todas las ventas con graficos y tablas
        {
            return View();
        }
    }
}