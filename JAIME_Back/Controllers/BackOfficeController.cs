﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datos;
using JAIME_Back.Models;

namespace JAIME_BackOffice.Controllers
{
    public class BackOfficeController : Controller
    {
        jaimeDBEntities db = new jaimeDBEntities();
        public ActionResult Index(string fechaDesde, string fechaHasta, string cliente, string comercio, string estado) //Dashboard del backoffice (se veran los comercios, coordinadores y deliveries)
        {
            BackOfficeViewModel model = new BackOfficeViewModel();
            model.ListaPedidos = db.Pedidos.ToList();
            List<AspNetUsers> ComerciosSinHorarios = new List<AspNetUsers>();
            List<AspNetUsers> ListaComercios = db.AspNetUsers.Where(x => x.TipoUsuario == TiposUsuarios.Comercio).ToList();
            foreach (var item in ListaComercios)
            {
                HorariosComercios com = db.HorariosComercios.Where(x => x.IdComercio == item.Id).FirstOrDefault();
                if (com == null)
                {
                    ComerciosSinHorarios.Add(item);
                }
            }

            //FILTROS
            if (!string.IsNullOrEmpty(fechaDesde) && !string.IsNullOrEmpty(fechaHasta))
            {
                ViewBag.fechaDesde = fechaDesde; //Los uso para pre cargar los filtros a la vuelta
                ViewBag.fechaHasta = fechaHasta;
                DateTime fecDesde = Convert.ToDateTime(fechaDesde);
                DateTime fecHasta = Convert.ToDateTime(fechaHasta);
                model.ListaPedidos = model.ListaPedidos.Where(x => x.Fecha >= fecDesde && x.Fecha <= fecHasta).ToList();
            }
            if (!string.IsNullOrEmpty(cliente))
            {
                ViewBag.cliente = cliente;
                model.ListaPedidos = model.ListaPedidos.Where(x => x.Cliente.ToLower() == cliente.ToLower() || x.Cliente.ToLower().Contains(cliente.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(comercio))
            {
                ViewBag.comercio = comercio;
                if (comercio != "Todos")
                {
                    model.ListaPedidos = model.ListaPedidos.Where(x => x.IdComercio == comercio).ToList();
                }
            }
            if (!string.IsNullOrEmpty(estado))
            {
                ViewBag.estado = estado;
                if (estado != "Todos")
                {
                    model.ListaPedidos = model.ListaPedidos.Where(x => x.Estado == estado).ToList();
                }
            }
            model.ListaPedidos = model.ListaPedidos.OrderByDescending(x => x.Prioridad).ThenByDescending(x => x.Fecha).ThenBy(x => x.Horario).ToList();

            model.ComerciosSinHorarios = ComerciosSinHorarios;
            model.ListaComercios = ListaComercios;
            return View(model);
        }

        public ActionResult AsignarHorariosComercio() //Asigna dias y horarios de comercios para comenzar a recibir pedidos.
        {
            AsignarHorariosViewModel model = new AsignarHorariosViewModel();
            model.ListaComercios = db.AspNetUsers.Where(x => x.TipoUsuario == TiposUsuarios.Comercio).ToList();
            //Cargo los 39 horarios distintos
            foreach (var hora in HorariosDisponibles.HorariosTodos)
            {
                model.ListaHorariosTodos.Add(hora.Hora);
            }
            return View(model);
        }

        public ActionResult CargarMediosPagoComercio() //Carga los medios de pago admitidos por el comercio
        {
            AsignarMediosPagoViewModel model = new AsignarMediosPagoViewModel();
            model.ListaComercios = db.AspNetUsers.Where(x => x.TipoUsuario == TiposUsuarios.Comercio).ToList();
            return View(model);
        }

        public ActionResult GuardarMediosPago(MediosPagoViewModel data) //Guarda los medios de pago del comercio seleccionado
        {
            //BORRAR MEDIOS DE PAGO ANTERIORES ANTES DE GUARDAR ESTOS
            try
            {
                List<MediosPagoComercios> MediosPagoComercio = db.MediosPagoComercios.Where(x => x.IdComercio == data.idComercio).ToList();
                if (MediosPagoComercio.Count() > 0)
                {
                    db.MediosPagoComercios.RemoveRange(MediosPagoComercio);
                    db.SaveChanges();
                }
                if (data.AceptaDebito)
                {
                    MediosPagoComercios medioPago = new MediosPagoComercios();
                    medioPago.MedioPago = MediosPago.Debito;
                    medioPago.IdComercio = data.idComercio;
                    db.MediosPagoComercios.Add(medioPago);
                    db.SaveChanges();

                }
                if (data.AceptaCredito)
                {
                    MediosPagoComercios medioPago = new MediosPagoComercios();
                    medioPago.MedioPago = MediosPago.Credito;
                    medioPago.IdComercio = data.idComercio;
                    db.MediosPagoComercios.Add(medioPago);
                    db.SaveChanges();

                }
                if (data.AceptaEfectivo)
                {
                    MediosPagoComercios medioPago = new MediosPagoComercios();
                    medioPago.MedioPago = MediosPago.Efectivo;
                    medioPago.IdComercio = data.idComercio;
                    db.MediosPagoComercios.Add(medioPago);
                    db.SaveChanges();

                }
                if (data.AceptaMercadopago)
                {
                    MediosPagoComercios medioPago = new MediosPagoComercios();
                    medioPago.MedioPago = MediosPago.MercadoPago;
                    medioPago.IdComercio = data.idComercio;
                    db.MediosPagoComercios.Add(medioPago);
                    db.SaveChanges();

                }
                if (data.AceptaTransferencia)
                {
                    MediosPagoComercios medioPago = new MediosPagoComercios();
                    medioPago.MedioPago = MediosPago.Transferencia;
                    medioPago.IdComercio = data.idComercio;
                    db.MediosPagoComercios.Add(medioPago);
                    db.SaveChanges();
                }
                db.SaveChanges();
                return Json(new { });
            }
            catch (Exception)
            {

                throw;
            }            
        }

        [HttpPost]
        public ActionResult GuardarEstadoPedido(GuardarEstadoPedidoViewModel data)
        {
            try
            {
                Pedidos pedido = db.Pedidos.Where(x => x.Id == data.idPedido).FirstOrDefault();
                pedido.Estado = data.estadoPedido;
                switch (data.estadoPedido)
                {
                    case EstadosPedidos.SinConfirmar:
                        pedido.Prioridad = PrioridadesPedidos.SinConfirmar;
                        break;
                    case EstadosPedidos.Confirmado:
                        pedido.Prioridad = PrioridadesPedidos.Confirmado;
                        break;
                    case EstadosPedidos.EnCamino:
                        pedido.Prioridad = PrioridadesPedidos.EnCamino;
                        break;
                    case EstadosPedidos.Retirado:
                        pedido.Prioridad = PrioridadesPedidos.Retirado;
                        break;
                    case EstadosPedidos.Entregado:
                        pedido.Prioridad = PrioridadesPedidos.Entregado;
                        //Creo un nuevo registro de una entrega a completar por el delivery, ya que tiene que devolver el dinero que obtuvo en la entrega al cliente
                        if (data.medioPago == MediosPago.Efectivo)
                        {
                            //Compruebo que ese pedido no este agregado ya a las entregas (voy a tener 1 registro de entregas por pedido)
                            EntregasDineroDeliveries pedidoActual = db.EntregasDineroDeliveries.Where(x => x.IdPedido == data.idPedido).FirstOrDefault();
                            if (pedidoActual == null)
                            {
                                EntregasDineroDeliveries record = new EntregasDineroDeliveries
                                {
                                    TotalPedido = data.monto,
                                    DineroEntregado = 0,
                                    IdDelivery = data.idDelivery,
                                    Fecha = data.fecha,
                                    IdPedido = data.idPedido
                                };
                                db.EntregasDineroDeliveries.Add(record);
                                db.SaveChanges();
                            }                          
                        }
                        break;
                    case EstadosPedidos.Cancelado:
                        pedido.Prioridad = PrioridadesPedidos.Cancelado;
                        break;
                    default:
                        break;
                }
                db.Entry(pedido).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Json(new { });
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult GuardarUbicacion(GuardarMapViewModel data)
        {
            try
            {
                Pedidos pedido = db.Pedidos.Where(x => x.Id == data.idPedido).FirstOrDefault();
                data.UrlMap = data.UrlMap.Replace("width=\"400\"", "width=\"300\"");
                data.UrlMap = data.UrlMap.Replace("width=\"600\"", "width=\"300\"");
                data.UrlMap = data.UrlMap.Replace("width=\"800\"", "width=\"300\"");
                data.UrlMap = data.UrlMap.Replace("height=\"450\"", "height=\"300\"");
                data.UrlMap = data.UrlMap.Replace("height=\"600\"", "height=\"300\"");
                pedido.Ubicacion = data.UrlMap;
                db.Entry(pedido).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Json(new { });
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult ObtenerUbicacion(int idPedido)
        {
            try
            {
                Pedidos pedido = db.Pedidos.Where(x => x.Id == idPedido).FirstOrDefault();
                if (pedido.Ubicacion != null)
                {
                    pedido.Ubicacion = pedido.Ubicacion.Replace("width=\"300\"", "width=\"600\"");
                    pedido.Ubicacion = pedido.Ubicacion.Replace("height=\"300\"", "height=\"600\"");
                    string ubi = pedido.Ubicacion;
                    return Json(new { ubi });
                }
                else
                {
                    return Json(new {  });
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult HorariosPorComercioId(string idComercio) //Cargar un nuevo comercio al sistema
        {
            //Compruebo si tenia horarios cargados
            List<HorariosViewModels> ListaHorariosPreCargados = new List<HorariosViewModels>();
            List<HorariosComercios> HorariosComercios = new List<HorariosComercios>();
            HorariosComercios = db.HorariosComercios.Where(x => x.IdComercio == idComercio).ToList();
            foreach (var item in HorariosComercios)
            {
                ListaHorariosPreCargados.Add(new HorariosViewModels
                {
                    IdComercio = item.IdComercio,
                    DiaAtencion = item.DiaAtencion,
                    HoraDesde = item.HoraDesde,
                    HoraHasta = item.HoraHasta
                });
            }
            int cantLunes = ListaHorariosPreCargados.Where(x => x.DiaAtencion == DiasSemana.Lunes).Count();
            int cantMartes = ListaHorariosPreCargados.Where(x => x.DiaAtencion == DiasSemana.Martes).Count();
            int cantMiercoles = ListaHorariosPreCargados.Where(x => x.DiaAtencion == DiasSemana.Miercoles).Count();
            int cantJueves = ListaHorariosPreCargados.Where(x => x.DiaAtencion == DiasSemana.Jueves).Count();
            int cantViernes = ListaHorariosPreCargados.Where(x => x.DiaAtencion == DiasSemana.Viernes).Count();
            int cantSabado = ListaHorariosPreCargados.Where(x => x.DiaAtencion == DiasSemana.Sabado).Count();
            int cantDomingo = ListaHorariosPreCargados.Where(x => x.DiaAtencion == DiasSemana.Domingo).Count();
            return Json(new { ListaHorariosPreCargados, cantLunes, cantMartes, cantMiercoles, cantJueves, cantViernes, cantSabado, cantDomingo });
        }


        public ActionResult GuardarHorarios(string[][] horarios) //Guarda los horarios de un comercio
        {
            //0 idComercio, 1 horaDesde, 2 horaHasta, 3 dia
            string idComercio = horarios[0][0];

            //borro horarios anteriores del comercio si tenia
            List<HorariosViewModels> ListaHorariosPreCargados = new List<HorariosViewModels>();
            List<HorariosComercios> HorariosComercios = db.HorariosComercios.Where(x => x.IdComercio == idComercio).ToList();
            if (HorariosComercios.Count() > 0)
            {
                db.HorariosComercios.RemoveRange(HorariosComercios);
                db.SaveChanges();
            }

            foreach (var item in horarios)
            {
                HorariosComercios horario = new HorariosComercios();
                horario.IdComercio = item[0];
                horario.HoraDesde = item[1];
                horario.HoraHasta = item[2];
                horario.DiaAtencion = item[3];
                db.HorariosComercios.Add(horario);
                db.SaveChanges();
            }
            return Json(new { });
        }

        public ActionResult VerComercios() //Ver todos los comercios
        {
            List<AspNetUsers> ListaComercios = db.AspNetUsers.Where(x => x.TipoUsuario == TiposUsuarios.Comercio).ToList();
            List<VerComerciosViewModel> model = new List<VerComerciosViewModel>();
            foreach (var item in ListaComercios)
            {
                model.Add(new VerComerciosViewModel
                {
                    NombreFantasia = item.NombreFantasia,
                    RazonSocial = item.RazonSocial,
                    Direccion = item.Direccion,
                    Email = item.Email,
                    CantPedidos = db.Pedidos.Where(x => x.IdComercio == item.Id).Count()
                });
            }
            model = model.OrderByDescending(x => x.CantPedidos).ToList();
            return View(model);
        }

        public ActionResult VerDeliveries() //Ver todos los delveries
        {
            List<AspNetUsers> ListaDeliveries = db.AspNetUsers.Where(x => x.TipoUsuario == TiposUsuarios.Delivery).ToList();
            List<VerDeliveriesViewModel> model = new List<VerDeliveriesViewModel>();
            foreach (var item in ListaDeliveries)
            {
                model.Add(new VerDeliveriesViewModel
                {
                    Nombre = item.Nombre,
                    Apellido = item.Apellido,
                    Direccion = item.Direccion,
                    Email = item.Email,
                    EntregasRealizadas = db.Pedidos.Where(x => x.Estado == EstadosPedidos.Entregado && x.IdDelivery == item.Id).Count()
                });
            }
            model = model.OrderByDescending(x => x.EntregasRealizadas).ToList();
            return View(model);
        }

        public ActionResult ConfigurarDiasYhorarios() //Configurar dias y horarios de jaime.
        {
            return View();
        }

        public ActionResult ConfigurarCostos() //Configurar costos de delivery. 3 rangos de precio.
        {
            return View();
        }
    }
}