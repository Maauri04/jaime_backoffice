﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datos;
using JAIME_Back.Models;

namespace JAIME_Back.Controllers
{
    public class PedidosController : Controller
    {
        jaimeDBEntities db = new jaimeDBEntities();
        // GET: Pedidos
        public ActionResult Index()
        {
            AgregarPedidoViewModel model = new AgregarPedidoViewModel();
            string idComercio = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault().Id;
            model.ListaMediosPago = db.MediosPagoComercios.Where(x => x.IdComercio == idComercio).ToList();
            return View(model);
        }

        public ActionResult AgregarPedido(PedidosViewModel data) //Agregar un pedido al comercio
        {
            try
            {
                Pedidos pedido = new Pedidos
                {
                    Cliente = data.Cliente,
                    Comentario = data.Comentario,
                    Direccion = data.Direccion,
                    Estado = data.Estado,
                    Fecha = data.Fecha,
                    Horario = data.Horario,
                    IdComercio = db.AspNetUsers.Where(x => x.UserName == data.Comercio).FirstOrDefault().Id,
                    IdDelivery = data.idDelivery,
                    MedioPago = data.MedioPago,
                    Monto = data.Monto,
                    Producto = data.Producto,
                    Telefono = data.Telefono,
                    Ubicacion = data.Ubicacion,
                    Prioridad = PrioridadesPedidos.SinConfirmar
                };
                db.Pedidos.Add(pedido);
                db.SaveChanges();
                return Json(new { });
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult DetallePedido(int idPedido) //Agregar un pedido al comercio
        {
            Pedidos pedido = db.Pedidos.Where(x => x.Id == idPedido).FirstOrDefault();
            AspNetUsers delivery = db.AspNetUsers.Where(x => x.TipoUsuario == TiposUsuarios.Delivery && x.Id == pedido.IdDelivery).FirstOrDefault();
            PedidosViewModel model = new PedidosViewModel
            {
                Cliente = pedido.Cliente,
                Comentario = pedido.Comentario,
                Comercio = db.AspNetUsers.Where(x => x.Id == pedido.IdComercio).FirstOrDefault().NombreFantasia,
                Delivery = delivery != null ? delivery.Nombre + " " + delivery.Apellido : "Sin asignar",
                Direccion = pedido.Direccion,
                Estado = pedido.Estado,
                Fecha = pedido.Fecha,
                Horario = pedido.Horario,
                idDelivery = pedido.IdDelivery,
                MedioPago = pedido.MedioPago,
                Monto = pedido.Monto,
                Producto = pedido.Producto,
                Telefono = pedido.Telefono,
                Ubicacion = pedido.Ubicacion != null ? pedido.Ubicacion.Replace("width=\"300\"", "width=\"820\"") : null
            };
            return View(model);
        }
    }
}