// Call the dataTables jQuery plugin
$(document).ready(function() {
    $('#dataTable').DataTable({
        "paging": true,
        "ordering": false,
        "info": true,
    });

    $('#dataTableDelivery').DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
		"searching": false
    });
});
